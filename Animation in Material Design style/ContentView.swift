//
//  ContentView.swift
//  Animation in Material Design style
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI
import PureSwiftUI

struct ContentView: View {
  @State private var isButtonsPresented = false
  @State private var plusButtonLocation: CGPoint = .zero
  var body: some View {
    ZStack {
      Color.gray
        .opacity(0.3)
        .ignoresSafeArea()
      VStack(spacing: 30) {
        Spacer()
        ButtonsStack(isButtonsPresented: $isButtonsPresented, centerLocation: $plusButtonLocation)
        Button {
          withAnimation(.easeInOut) {
            isButtonsPresented.toggle()
          }
        } label: {
          Circle()
            .fill(.blue)
            .overlay(
              Image(systemName: "plus")
                .foregroundColor(.white)
            )
            .frame(width: 48, height: 48)
        }.geometryReader { geo in
          plusButtonLocation = geo.globalCenter
        }
        
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

struct MaterialDesignButton: View {
  
  struct Configuratation: Hashable {
    let systemName: String
    let text: String
  }
  
  var action: () -> Void = { }
  let configuration: Configuratation
  
  var body: some View {
    Button {
      action() // Do something
    } label: {
      VStack {
        Circle()
          .fill(.white)
          .frame(width: 48, height: 48)
          .overlay(
            Image(systemName: configuration.systemName)
              .foregroundColor(.black)
          )
        Text(configuration.text)
          .font(.caption)
          .foregroundColor(.white)
          .frame(minWidth: 100)
      }
    }
    
  }
}

struct ButtonsStack: View {
  var buttonConfigs: [[MaterialDesignButton.Configuratation]] = [
    [
      .init(systemName: "star", text: "Favorite"),
      .init(systemName: "trash", text: "Delete"),
      .init(systemName: "square.and.arrow.up", text: "Share")
    ],
    [
      .init(systemName: "tag", text: "Tag"),
      .init(systemName: "text.bubble", text: "Comment"),
    ]
  ]
  
  @Binding var isButtonsPresented: Bool
  @Binding var centerLocation: CGPoint
  
  var body: some View {
    VStack(spacing: 30) {
      ForEach(buttonConfigs, id: \.self) { row in
        HStack {
          ForEach(row, id: \.self) { config in
            MaterialDesignButton(configuration: config)
              .offsetToPositionIfNot(isButtonsPresented, centerLocation)
              .opacity(isButtonsPresented ? 1 : 0)
          }
        }
      }
    }
  }
}
