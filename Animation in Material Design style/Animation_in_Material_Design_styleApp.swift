//
//  Animation_in_Material_Design_styleApp.swift
//  Animation in Material Design style
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

@main
struct Animation_in_Material_Design_styleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
